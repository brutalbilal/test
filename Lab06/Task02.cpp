#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

using namespace std;

vector<double> timeRecord;

bool moveMin(vector<int> &in, vector<int> &out) {

	clock_t start = clock();

	out = in;

	bool flag = true;
	int maxAtIndex = 0, indexDone = 1;

	while (flag) {

		flag = false;

		for (int i = 1; i < out.size() - indexDone; i++) {

			if (out[maxAtIndex] > out[i]) {
				flag = true;
			}
			else maxAtIndex = i;

		}

		swap(out[maxAtIndex], out[out.size() - indexDone - 1]);
		indexDone++;
		maxAtIndex = 0;


	}

	timeRecord.push_back((double)(clock() - start) / CLOCKS_PER_SEC);

	return true;

}

bool testCase(vector<int> mySort, vector<int> builtInSort) {

	for (int i = 0; i < mySort.size(); i++) {
		if (mySort[i] != builtInSort[i]) return false;
	}

	return true;

}

bool testMoveMin() {

	vector<int> in;
	vector<int> out = in;

	int num;

	for (int i = 0; i < 10000; i++) {
		num = rand() % 100;
		in.push_back(num);
	}

	moveMin(in, out);

	vector<int> builtInSort = in;

	sort(builtInSort.begin(), builtInSort.end());

	return true;

}


int main() {

	for (int i = 0; i < 1; i++) {
		srand(time(NULL) + i);
		testMoveMin();
	}

	double best = *min_element(timeRecord.begin(), timeRecord.end());
	double worst = *max_element(timeRecord.begin(), timeRecord.end());
	double sum = 0;

	for (double a : timeRecord) sum += a;

	cout << "Best Case Time: " << best << endl;
	cout << "Worst Case Time: " << worst << endl;
	cout << "Average Case Time: " << sum / timeRecord.size() << endl;

	system("pause");

	return 0;

}

