#include <iostream>
#include <vector>
#include <time.h>
#include <algorithm>

using namespace std;

bool testMoveMin(vector<int> &in, vector<int> & out){
	out = in;
	for (int i = 0; i < in.size()-2; ++i) {
		for (int j = 0; j < in.size()-2; ++j) {
			//Copy Vector is being Sorted.
			if (out[j] > out[j + 1]) {
				std::swap(out[j],out[j + 1]);
			}
		}
	}
	return true;
};
/// MoveMin Problem using 2 Copy Vectors
void moveMin()
{

	vector<int> in;
	vector<int> out = in;
	///Original Vector is inititalizing;
	cout << "Printing Original  ";
	for (int i = 0; i < 10; i++) {
		int num = rand() % 100;
		in.push_back(num);
		cout << num << " ";
	}
	cout << endl;
	testMoveMin(in, out);
	cout << "Sorted through Algorithm  ";
	for (int i : out) cout << i << " ";
	cout << endl;
	cout << "Printing Sort Func:  ";
	vector<int> inSort = in;
	sort(inSort.begin(), inSort.end());
	for (int i : inSort) cout << i << " ";
	cout << endl;
	for (int i = 0; i < inSort.size(); i++) {
		if (inSort[i] != out[i]) {
			cout << "Matched."<< endl; break;
		}
		else {
			cout << "Not Matched"<< endl;
		}
		break;
	}
}

bool test(vector<int> inSort, vector<int> out) 
{

	for (int i = 0; i < inSort.size(); i++) {
		if (inSort[i] != out[i]) return false;
	}
	return true;
}

int main() {
	for (int c = 0; c < 5; ++c) {
		srand((unsigned)time(NULL) + c);
		moveMin();
	}
	system("Pause");
	return 0;
}